---
path: /events/opentalks-ep2/
date: '2024-09-03'
datestring: '3 September 2024'
author: 'noelty'
title: 'GSoC Orientation Phase 1'
cover: './opentalks-ep2.jpg'
name: 'Noel Tom Santhosh'
---

![Poster](./opentalks-ep2.jpg)

## Open Talks Episode 2: Google Summer of Code (GSoC)

Join us for **Episode 2 of Open Talks** presented by _FOSSNSS_, an enlightening orientation session on **Google Summer of Code (GSoC)**.

_GSoC_ connects students with open-source projects to gain coding experience and contribute to the community. Discover how you can be a part of this open-source world through this insightful session. Don't miss this opportunity✨

### Session Details:

-   **Speaker🎙️**: Unnimaya K
-   **Date🗓️**: 3rd September 2024
-   **Time⏳**: 4:30 PM
-   **Venue📍**: CSE Seminar Hall

Don't miss this chance to explore the world of GSoC and make an impact🌟
